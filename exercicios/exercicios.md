# Exercício 1

### Verficar instalação Docker

Iremos verificar a instalação do Docker em todos os hosts.

```sh
$ docker --version
$ docker ps
```

### Logar no registro

Depois de instalar o Docker em todos os hosts, será preciso fazer o login do docker engine do host no registro.
Para isso, temos que editar o arquivo /etc/docker/daemon.json, e inserir o parâmetro de registro inseguro. 
```sh
$ vi /etc/docker/daemon.json
{
	"insecure-registries" : ["harbor.rancher.ailos.coop.br"]
}
```
Após inserir o registro inseguro, devido ao certificado SSL, é preciso reiniciar o docker e fazer o login, usando seu usuário do treinamento.
```sh
$ systemctl restart docker
$ docker login harbor.rancher.ailos.coop.br
	Login sucessful
```
Para testar se está tudo certo, somente no host A, iremos criar uma imagem para enviar para nosso repositorio no registro. Iremos baixar a imagem do node:alpine, colocar a tag correta para então fazer o push para o nosso repositório privado. 

Onde estiver escrito {user}, é preciso substituir pelo seu nome de usuario no registro.

```sh
$ docker pull node:alpine
$ docker tag node:alpine harbor.rancher.ailos.coop.br/aluno1/node:alpine
$ docker push harbor.rancher.ailos.coop.br/aluno1/node:alpine
```
### Limpar o host
Algumas vezes é preciso sanitizar o host, e deixar ele apto para receber carga novamente. Para isso, muitas vezes é preciso remover o docker, suas dependências, arquivos do kubernetes, entre outros.

Não itemos limpar o host nesse passo, mas os comandos estão aqui como referência.
```sh
$ docker rm -f $(docker ps -qa)
$ docker rmi -f $(docker images -q)
$ docker volume rm $(docker volume ls)
$ systemctl stop docker
$ for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do umount $mount; done
$ rm -rf /etc/ceph /etc/cni /etc/kubernetes /opt/cni /opt/rke /run/secrets/kubernetes.io /run/calico /run/flannel /var/lib/calico /var/lib/etcd /var/lib/cni /var/lib/kubelet /var/lib/rancher/rke/log /var/log/containers /var/log/pods /var/run/calico
$ systemctl start docker
```

### FirewallD
Talvez o firewall esteja ativado nos host's, será preciso desativar.
```sh
$ iptables -F
$ systemctl stop firewalld
$ systemctl disable firewalld

$ vi /etc/selinux/config
	SELINUX=disabled
$ reboot
$ getenforce
```

# Exercício 2

### Fazer build das imagens, rodar docker-compose.

Nesse exercício iremos construir as imagens dos containers que iremos usar, colocar elas para rodar em conjunto com o docker-compose. 

Entrar no host A, e instalar os pacotes abaixo, que incluem Git, Python, Pip e o Docker-compose.
```sh
$ sudo su
$ yum install git -y
$ yum install epel-release -y
$ yum install -y python-pip
$ pip install docker-compose
$ yum upgrade python* -y
$ pip uninstall docker-compose
$ pip install docker-compose==1.9.0
```
Com os pacotes instalados, agora iremos baixar o código fonte e começaremos a fazer os build's e rodar os containers.
```sh
$ cd /root
$ git clone https://github.com/jonathanbaraldi/treinamento-kubernetes
$ cd treinamento-kubernetes/dia-1-2/app
```
#### Container=REDIS
Iremos fazer o build da imagem do Redis para a nossa aplicação.
```sh
$ cd redis
$ docker build -t harbor.rancher.ailos.coop.br/aluno1/redis .
$ docker run -d --name redis -p 6379:6379 harbor.rancher.ailos.coop.br/aluno1/redis
$ docker ps
$ docker logs redis
```
Com isso temos o container do Redis rodando na porta 6379.

#### Container=NODE
Iremos fazer o build do container do NodeJs, que contém a nossa aplicação.
```sh
$ cd ../node
$ docker build -t harbor.rancher.ailos.coop.br/aluno1/node .
```
Agora iremos rodar a imagem do node, fazendo a ligação dela com o container do Redis.
```sh
$ docker run -d --name node -p 8080:8080 --link redis harbor.rancher.ailos.coop.br/aluno1/node
$ docker ps 
$ docker logs node
```
Com isso temos nossa aplicação rodando, e conectada no Redis. A api para verificação pode ser acessada em /redis.

#### Container=NGINX
Iremos fazer o build do container do nginx, que será nosso balanceador de carga.
```sh
$ cd ../nginx
$ docker build -t harbor.rancher.ailos.coop.br/aluno1/nginx .
```
Criando o container do nginx a partir da imagem e fazendo a ligação com o container do Node
```sh
$ docker run -d --name nginx -p 80:80 --link node harbor.rancher.ailos.coop.br/aluno1/nginx
$ docker ps
```
Podemos acessar então nossa aplicação nas portas 80 e 8080 no ip da nossa instância.

Iremos acessar a api em /redis para nos certificar que está tudo ok, e depois iremos limpar todos os containers e volumes.
```sh
$ docker rm -f $(docker ps -a -q)
$ docker volume rm $(docker volume ls)
```


#### DOCKER-COMPOSE
Nesse exercício que fizemos agora, colocamos os containers para rodar, e interligando eles, foi possível observar  como funciona nossa aplicação que tem um contador de acessos.
Para rodar nosso docker-compose, precisamos remover todos os containers que estão rodando e ir na raiz do diretório para rodar.

É preciso editar o arquivo docker-compose.yml, onde estão os nomes das imagens e colocar o seu nome de usuário.

Linha 8 = harbor.rancher.<>/{user}/nginx
Linha 18 = image: harbor.rancher.<>/{user}/redis
Linha 47 = image: harbor.rancher.<>/{user}/node

Após alterar e colocar o nome correto das imagens, rodar o comando de up -d para subir a stack toda.

```sh
$ cd ..
$ vi docker-compose.yml
$ docker-compose -f docker-compose.yml up -d
$ curl <ip>:80 
	----------------------------------
	This page has been viewed 29 times
	----------------------------------
```
Se acessarmos o IP:80, iremos acessar a nossa aplicação. Olhar os logs pelo docker logs, e fazer o carregamento do banco em /load

Para terminar nossa aplicação temos que rodar o comando do docker-compose abaixo:
```sh
$ docker-compose down
```





# Exercício 3

### Instalar Rancher - Single Node

Nesse exercício iremos instalar o Rancher 2.2.5 versão single node. Isso significa que o Rancher e todos seus componentes estão em um container. 

Entrar no host A, que será usado para hospedar o Rancher Server. Iremos verficar se não tem nenhum container rodando ou parado, e depois iremos instalar o Rancher.
```sh
$ docker ps -a
$ docker run -d --name rancher --restart=unless-stopped -p 80:80 -p 443:443 rancher/rancher:v2.2.5
```
Com o Rancher já rodando, irei adicionar a entrada de cada DNS para o IP de cada máquina.

```sh
$ aluno1.rancher.ailoshml.coop.br = IP do host A
```

# Exercício 4

### Criar cluster Kubernetes

Nesse exercício iremos criar um cluster Kubernetes. Após criar o cluster, iremos instalar o kubectl no host A, e iremos usar para interagir com o cluster.

Seguir as instruções na aula para fazer o deployment do cluster.
Após fazer a configuração, o Rancher irá exibir um comando de docker run, para adicionar os host's.

Adicionar o host B e host C. 

Pegar o seu comando no seu rancher.
```sh
docker run -d --privileged --restart=unless-stopped --net=host -v /etc/kubernetes:/etc/kubernetes -v /var/run:/var/run rancher/rancher-agent:v2.2.5 --server https://aluno1.rancher.ailoshml.coop.br --token snv2tz285b2772xn84psnx5v5mkh4wshrn566h5bfblw4trxmm7sr2 --ca-checksum ef196e1dd986219dc56aa6cd47a2782581b413040075c99aa0b29a5ba5c05d29 --node-name node2 --etcd --controlplane --worker



```
Será um cluster com 2 nós.
Navegar pelo Rancher e ver os painéis e funcionalidades.

# Exercício 5

### Instalar kubectl no host A

Agora iremos instalar o kubectl, que é a CLI do kubernetes. Através do kubectl é que iremos interagir com o cluster.
```sh
$ sudo su
$ cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
$ yum install -y kubectl
$ kubectl
```

Com o kubectl instalado, pegar as credenciais de acesso no Rancher e configurar o kubectl.

```sh
$ vi ~/.kube/config
$ kubectl get nodes
```




# Exercício 6

### Traefik - DNS

O Traefik é a aplicação que iremos usar como ingress. Ele irá ficar escutando pelas entradas de DNS que o cluster deve responder. Ele possui um dashboard de  monitoramento e com um resumo de todas as entradas que estão no cluster.
```sh
$ kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-rbac.yaml
$ kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-ds.yaml
$ kubectl --namespace=kube-system get pods
```
Agora iremos configurar o DNS pelo qual o Traefik irá responder. No arquivo ui.yml, localizar a url, e fazer a alteração. Após a alteração feita, iremos rodar o comando abaixo para aplicar o deployment no cluster.
```sh
$ cd treinamento-kubernetes/exercicios/
$ kubectl apply -f ui.yml
```
#### Teste do Chesse
Iremos fazer o deployment das aplicações teste do Trafik. São aplicações com nomes de queijo. 
```sh
$ kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/cheese-deployments.yaml
$ kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/cheese-services.yaml
```
Precisamos configurar os ingress, para que as nossas aplicações respondam pelo DNS delas.
```sh
$ kubectl apply -f cheese.yml
```
Após aplicar o deployment, acessar o traefik, e acessar as url's das aplicações de queijo.



# Exercício 7

### Graylog - LOG

O Graylog é a aplicação que iremos usar como agregador de logs do cluster. Os logs dos containers podem ser vistos pelo Rancher, é um dos níveis de visualização. Pelo Graylog temos outros funcionalidades, e também é possível salvar para posterior pesquisa, e muitas outras funcionalidades.

Para instalar o Graylog, iremos aplicar o template dele, que está em graylog.yml. Para isso, é preciso que sejam editados 2 pontos no arquivo.

Linha 177 - value: http://graylog.{user}.rancher.ailoshtml.coop.br/api
Linha 234 - host: graylog.{user}.rancher.ailoshtml.coop.br

Substituir o {user}, pelo nome do aluno. Após substituir, aplicar e entrar no Graylog para configurar.
```sh
$ kubectl apply -f graylog.yml
```
Seguir os passos do instrutor para configuração do Graylog.


# Exercício 8

### Grafana - MONITORAMENTO

O Grafana/Prometheus é a stack que iremos usar para monitoramento. O Deployment dela será feito pelo Catálogo de Apps.
Iremos configurar seguindo as informações do instrutor, e fazer o deployment.

Será preciso altear os DNS das aplicações para que elas fiquem acessíveis.

Após o deploymnet, entrar no Grafana e Prometheus e mostrar seu funcionamento.




# Exercício 9

### Whoami - APP EXEMPLO

Neste exemplo de aplicação, iremos criar a nossa imagem, e enviar para o registro. Após isto, iremos configurar o arquivo app.yml, que contem todo deployment da nossa aplicação. 

Após o push para o registro, rodar os comandos abaixo para que a aplicação exemplo rode no cluster.

```sh
$ docker pull cizixs/whoami
$ docker tag cizixs/whoami harbor.rancher.ailos.coop.br/aluno1/whoami
$ docker push harbor.rancher.ailos.coop.br/aluno1/whoami

$ kubectl apply -f app.yml
```






# Exercício 10

### CronJob

O tipo de serviço como CronJob é um serviço igual a uma cron, porém é executado no cluster kubernetes. Você agenda um pod que irá rodar em uma frequência determinada de tempo. Pode ser usado para diversas funções, como executar backup's dos bancos de dados.

Nesse exemplo, iremos executar um pod, com um comando para retornar uma mensagem de tempos em tempos, a mensagem é "Hello from the Kubernetes cluster"

```sh
$ kubectl apply -f cronjob.yml
	cronjob "hello" created
```
Depois de criada a cron, pegamos o estado dela usando:
```sh
$ kubectl get cronjob hello
NAME      SCHEDULE      SUSPEND   ACTIVE    LAST-SCHEDULE
hello     */1 * * * *   False     0         <none>
```
Ainda não existe um job ativo, e nenhum agendado também.
Vamos esperar por 1 minutos ate o job ser criado:
```sh
$ kubectl  get jobs --watch
```
Entrar no Rancher para ver os logs e a sequencia de execucao.




# Exercício 11

### ConfigMap

O ConfigMap é um tipo de componente muito usado, principalmente quando precisamos colocar configurações dos nossos serviços externas aos contâiners que estão rodando a aplicação. 

Nesse exemplo, iremos criar um ConfigMap, e iremos acessar as informações dentro do container que está a aplicação.
```sh
$ kubectl apply -f configmap.yml
```
Agora iremos entrar dentro do container e verificar as configurações definidas no ConfigMap.



# Exercício 12

### Secrets

Os secrets são usados para salvar dados sensitivos dentro do cluster, como por exemplo senhas de bancos de dados. Os dados que ficam dentro do secrets não são visíveis a outros usuários, e também podem ser criptografados por padrão no banco.

Iremos criar os segredos.

```sh
$ echo -n "<nome-aluno>" | base64
am9uYXRoYW4=
$ echo -n "<senha>" | base64
am9uYXRoYW4=
```

Agora vamos escrever o secret com esses objetos. Após colocar os valores no arquivo secrets.yml, aplicar ele no cluster.
```sh
$ kubectl apply -f secrets.yml
```
Agora com os secrets aplicados, iremos entrar dentro do container e ver como podemos recuperar seus valores.













# Exercício 13

### Liveness

Nesse exercício do liveness, iremos testar como fazer para dizer ao kubernetes, quando recuperar a nossa aplicação, caso alguma coisa aconteça a ela.
```js
http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) { 
	duration := time.Now().Sub(started) 
	if duration.Seconds() > 10 { 
		w.WriteHeader(500) 
		w.Write([]byte(fmt.Sprintf("error: %v", duration.Seconds()))) 
	} else { 
		w.WriteHeader(200) 
		w.Write([]byte("ok")) 
	} 
})
```
O código acima, está dentro do container que iremos rodar. Nesse código, vocês podem perceber que tem um IF, que irá fazer que de vez em quando a aplicação responda dando erro. 

Como a aplicação irá retornar um erro, o serviço de liveness que iremos usar no Kubernetes, ficará verificando se a nossa aplicação está bem, e como ela irá falhar de tempos em tempos, o kubernetes irá reiniciar o nosso serviço.

```sh
$ kubectl apply -f liveness.yml 
	Depois de 10 segundos, verificamos que o container reiniciou. 
$ kubectl describe pod liveness-http 
$ kubectl get pod liveness-http 
```



# Exercício 14

### Rolling-update


Nesse exercício de rolling-update, iremos fazer o deployment do nginx na versão 1.7.9. Sendo 5 pods rodando a aplicação.

Iremos rodar o comando de rolling update, para atualizar para a versão 1.9.1. Dessa forma o Kubernetes irá rodar 1 container com a nova versão, e para um container com a antiga versão. Ele irá fazer isso para cada um dos containers, substituindo todos eles, e não havendo parada de serviço.

```sh
$ kubectl apply -f rolling-update.yml
```
Nesse arquivo o nginx está na versão 1.7.9
Para atualizar a imagem do container para 1.9.1 iremos usar o kubectl rolling-update e especificar a nova imagem.
```sh
$ kubectl  rolling-update my-nginx --image=nginx:1.9.1
	Created my-nginx-ccba8fbd8cc8160970f63f9a2696fc46
```
Em outra janela, você pode ver que o kubectl adicionou o label do deployment para os pods, que o valor é um hash da configuração, para distinguir os pods novos dos velhos
```sh
$ kubectl get pods -l app=nginx -L deployment
```



# Exercício 15

### Autoscaling

Iremos executar o tutorial oficial para autoscaling.

https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/#before-you-begin

Para isso iremos rodar e expor o php-apache server

```sh
$ kubectl run php-apache --image=k8s.gcr.io/hpa-example --requests=cpu=200m --expose --port=80
```

Agora iremos fazer a criação do Pod Autoscaler

```sh
$ kubectl apply -f hpa.yaml
```

Iremos pegar o HPA

```sh
$ kubectl get hpa
```

### Autoscaling - Aumentar a carga

Agora iremos aumentar a carga no pod contendo o apache em php.

```sh
$ kubectl run -i --tty load-generator --image=busybox /bin/sh
Hit enter for command prompt
$ while true; do wget -q -O- http://php-apache.default.svc.cluster.local; done
```

Agora iremos em outro terminal, com o kubectl, verificar como está o HPA, e também no painel do Rancher. 

```sh 
$ kubectl get hpa
$ kubectl get deployment php-apache
```

# Exercício 16

### Volumes

Para fazermos os exercícios do volume, iremos fazer o deployment do pod com o volume, que estará apontando para um caminho no host.

Fazer o deployment do Longhorn.

```sh 
# Verificar se o iscsi está instalado, nos nodes B e C
$ yum install iscsi-initiator-utils -y

$ kubectl apply -f volume.yml
$ kubectl apply -f mariadb-longhorn-volume.yml
```



# Exercício 17

### Declarar políticas de rede

https://kubernetes.io/docs/tasks/administer-cluster/declare-network-policy/

https://github.com/ahmetb/kubernetes-network-policy-recipes

https://ahmet.im/blog/kubernetes-network-policy/

Para ver como funciona as policies de rede do kubernetes, iremos criar um deployment do nginx e expor ele via serviço.

```sh
$ kubectl run nginx --image=nginx --replicas=2
	deployment.apps/nginx created
$ kubectl expose deployment nginx --port=80
	service/nginx exposed
```

Iremos ter 2 pods no namespace default rodando o nginx e expondo atraves do serviço chamado nginx.


#### Testando o serviço

Iremos agora testar o acesso do nosso pod no nginx para os outros pods. Para acessar o serviço iremos rodar um prompt de dentro de um container busybox e iremos acessar o nginx

```sh
$ kubectl run busybox --rm -ti --image=busybox /bin/sh
	Waiting for pod default/busybox-472357175-y0m47 to be running, status is Pending, pod ready: false

	Hit enter for command prompt

$ wget --spider --timeout=1 nginx
	Connecting to nginx (10.100.0.16:80)
$ exit
```

Vendo o IP do POD do nginx respondendo, conseguimos ver que existe a conexão.

Agora iremos aplicar as regras no arquivo **nginx-policy.yml**.

```sh
$ kubectl create -f nginx-policy.yaml
```

Após as regras aplicadas, iremos testar novamente a conexão do pod busybox com o nginx.

```sh
$ kubectl run busybox --rm -ti --image=busybox /bin/sh
	Waiting for pod default/busybox-472357175-y0m47 to be running, status is Pending, pod ready: false

	Hit enter for command prompt

$ wget --spider --timeout=1 nginx
	Connecting to nginx (10.100.0.16:80)
	wget: download timed out
$ exit
```

Podemos perceber que está dando timeout na conexão, o container do busybox não consegue conectar no nginx

Agora iremos criar novamente o container do busybox , mas iremos colocar o label de access=true para que o POD possa se conectar.

```sh
$ kubectl run busybox --rm -ti --labels="access=true" --image=busybox /bin/sh
	Waiting for pod default/busybox-472357175-y0m47 to be running, status is Pending, pod ready: false

	Hit enter for command prompt

$ wget --spider --timeout=1 nginx
	Connecting to nginx (10.100.0.16:80)
$ exit
```



# Exercício 18

### LABEL E SELETORES

Nesse exercício iremos colocar o label disktype, com valor igual a "ssd" no nó B do nosso cluster. 
Esse exercício serve para demonstrar como podemos usar o kubernetes para organizar onde irão rodar os containers. Neste exemplo estamos usando disco SSD em 1 máquina, poderia ser ambiente diferente, recursos de rede diferentes também, etc.

```sh
$ kubectl label nodes <your-node-name> disktype=ssd

$ kubectl label nodes node1 disktype=ssd

$ kubectl apply -f node-selector.yml
```




# Exercício 19


### Kubeless

https://kubeless.io

Para instalar o Kubeless em nosso cluster, iremos rodar os comandos abaixo.

```sh
$ export RELEASE=$(curl -s https://api.github.com/repos/kubeless/kubeless/releases/latest | grep tag_name | cut -d '"' -f 4)
$ kubectl create ns kubeless
$ kubectl create -f https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless-$RELEASE.yaml

$ kubectl get pods -n kubeless
NAME                                           READY     STATUS    RESTARTS   AGE
kubeless-controller-manager-567dcb6c48-ssx8x   1/1       Running   0          1h

$ kubectl get deployment -n kubeless
NAME                          DESIRED   CURRENT   UP-TO-DATE   AVAILABLE   AGE
kubeless-controller-manager   1         1         1            1           1h

$ kubectl get customresourcedefinition
NAME                          AGE
cronjobtriggers.kubeless.io   1h
functions.kubeless.io         1h
httptriggers.kubeless.io      1h
```

Depois de instalado no cluster, iremos agora instalar a linha de comando no mesmo local onde usamos nosso kubectl.

```sh
$ export OS=$(uname -s| tr '[:upper:]' '[:lower:]')
$ curl -OL https://github.com/kubeless/kubeless/releases/download/$RELEASE/kubeless_$OS-amd64.zip && unzip kubeless_$OS-amd64.zip && sudo mv bundles/kubeless_$OS-amd64/kubeless /usr/local/bin/
```

Para verificar se foi instalado corretamente, iremos rodar:

```sh
$ kubeless function ls
```

### Kubeless - Função exemplo

Para fazer o deploy da função, iremos usar o arquivo de modelo exemplo **test.py** 

```sh
$ kubeless function deploy hello --runtime python2.7 --from-file test.py --handler test.hello
$ kubectl get functions
$ kubeless function ls
```

Para chamar a função podemos fazer da seguinte forma:
```sh
$ kubeless function call hello --data 'Hello world!'
```


Agora iremos aplicar a função através do arquivo YML, contendo todos os dados da função

```sh
$ kubectl apply -f function-nodejs.yml
$ kubeless function call hello-nodejs
```

### Kubeless UI 

https://github.com/kubeless/kubeless-ui

O Kubeless possui uma UI para facilitar a operação. Para instalar ela, iremos rodar:

```sh
$ kubectl create -f https://raw.githubusercontent.com/kubeless/kubeless-ui/master/k8s.yaml
```

Na interface do Rancher, iremos acessar pelo IP:PORTA no qual a UI foi instalada. E iremos executar alguns exemplos para entender seu funcionamento.


### Kubeless Simple API 

https://github.com/kubeless/functions/tree/master/incubator/rest-api

Iremos usar os arquivos **crypto.py**, e **requirements.txt** para pegar o valor do Bitcoin através de uma função.

```sh 
$ kubeless function deploy crypto --from-file crypto.py --handler crypto.handler --runtime python2.7 --dependencies requirements.txt
$ kubeless function call crypto --data '{"crypto": "bitcoin"}'

curl --data '{"crypto": "bitcoin"}' \
  --header "Host: crypto.aluno1.rancher.ailoshml.coop.br" \
  --header "Content-Type:application/json" \
  crypto.aluno1.rancher.ailoshml.coop.br
```


As trigers HTTP podem ser consultadas na documentação oficial.

https://github.com/kubeless/http-trigger


# Exercício 20

### HELM


```sh 
$ curl -LO https://git.io/get_helm.sh
$ chmod 700 get_helm.sh
$ ./get_helm.sh
$ helm init
$ helm init --upgrade


$ kubectl create serviceaccount --namespace kube-system tiller
$ kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
$ kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'


$ helm search

$ helm repo update

$ helm install stable/redis
```


```sh
$ helm create endpoints
$ helm install --name endpoints /root/treinamento-kubernetes/endpoints/
```






# Exercício 21

### ResourceQuota

Definir as quotas

```sh

$ kubectl create namespace quota-object-example
$ kubectl apply -f resouce-quota.yml

$ kubectl get resourcequota object-quota-demo --namespace=quota-object-example --output=yaml
```

### Criar primeiro PVC

```sh
$ kubectl apply -f resouce-quota-pvc.yml
$ kubectl get persistentvolumeclaims --namespace=quota-object-example
```

### Criar segundo PVC

```sh
$ kubectl apply -f resouce-quota-pvc-2.yml
$ kubectl get persistentvolumeclaims --namespace=quota-object-example

# persistentvolumeclaims "pvc-quota-demo-2" is forbidden:
# exceeded quota: object-quota-demo, requested: persistentvolumeclaims=1,
# used: persistentvolumeclaims=1, limited: persistentvolumeclaims=1
```

Objetos que podem ser restringidos por Quotas:

- pods = Pod
- services = Service
- replicationcontrollers = ReplicationController
- resourcequotas = ResourceQuota
- secrets = Secret
- configmaps = ConfigMap
- persistentvolumeclaims = PersistentVolumeClaim
- services.nodeports = Service of type NodePort
- services.loadbalancers = Service of type LoadBalancer

### Limpar ambiente

```sh
$ kubectl delete namespace quota-object-example
```

### ResourceQuota com CPU e MEMÓRIA

```sh 
$ kubectl create namespace quota-mem-cpu-example

$ kubectl apply -f resouce-quota-mem-cpu.yml

$ kubectl get resourcequota mem-cpu-demo --namespace=quota-mem-cpu-example --output=yaml

$ kubectl apply -f resouce-quota-mem-cpu-pod.yml

$ kubectl get resourcequota mem-cpu-demo --namespace=quota-mem-cpu-example --output=yaml

$ kubectl apply -f resouce-quota-mem-cpu-pod-2.yml

# Error from server (Forbidden): error when creating "examples/admin/resource/quota-mem-cpu-pod-2.yaml":
# pods "quota-mem-cpu-demo-2" is forbidden: exceeded quota: mem-cpu-demo,
# requested: requests.memory=700Mi,used: requests.memory=600Mi, limited: requests.memory=1Gi


$ kubectl delete namespace quota-mem-cpu-example
```


# Exercício 22

### K8S-Cleaner

* Cleans up exited containers and dangling images/volumes running as a DaemonSet (`docker-clean.yml`).
* Cleans up old replica sets, finished jobs and unrecycled evicted pods as a CronJob (`k8s-clean.yml`).

```sh
$ kubectl --context CONTEXT -n kube-system apply -f rbac.yml
$ kubectl --context CONTEXT -n kube-system apply -f docker-clean.yml
$ kubectl --context CONTEXT -n kube-system apply -f k8s-clean.yml

```

# EXERCÍCIO 23

## DOCUMENTO PDF - DIVING DEEP INTO KUBERNETES NETWORKING



# Exercício 24

### DR Rancher - Backup

Iremos executar um plano de DR do Rancher, onde iremos fazer o backup do banco do rancher, e depois iremos fazer o restore do Rancher na mesma máquina onde ele está.

Para isso, iremos parar o container do Rancher server, criar o volume com a cópia do banco, e gerar o arquivo com o backup. Feito isso, iremos reiniciar o Rancher para continuar de onde havíamos parado.  


```sh
$ docker stop rancher
$ docker create --volumes-from rancher --name rancher-data-25-7-2018 rancher/rancher:stable
$ docker run  --volumes-from rancher-data-25-7-2018 -v $PWD:/backup alpine tar zcvf /backup/rancher-data-backup-rancher2-25-7-2018.tar.gz /var/lib/rancher  
$ docker start rancher
```

Foi gerado um arquivo ''rancher-data-backup-rancher2-25-7-2018.tar.gz'', que contém o backup do Rancher Server.

### DR Rancher - Restore

Para que possamos fazer o restore do Rancher, iremos parar nosso Rancher rodando, e iremos remover o container dele, bem como os volumes todos, limpando qualquer registro que ele possa ter deixado.

Para limpar, iremos usar:
```sh
$ docker rm -f $(docker ps -a -q)
$ docker volume rm $(docker volume ls)
```
Uma vez limpo o host, iremos agora colocar para rodar um Rancher inteiramente novo:
```sh
$ docker ps -a
$ docker run -d --name rancher --restart=unless-stopped -p 80:80 -p 443:443 rancher/rancher:v2.2.5
```

Com esse novo Rancher Server rodando, iremos acessar sua url para certificar que é uma nova instalação. 

O que irá nos dizer logo na tela inicial, é se o Rancher pedir para criar a senha de administrador, se aparecer essa tela, é uma instalação nova.

Após se certifcar que realmente é uma nova instalação, iremos parar o container que está executando, e fazer o restore do banco que está no arquivo da etapa anterior.

Verificar através do comando de docker logs, que o Rancher foi completamente instalado.

```sh
$ docker stop rancher
$ docker run  --volumes-from rancher -v $PWD:/backup  alpine sh -c "rm /var/lib/rancher/* -rf  &&  tar zxvf /backup/rancher-data-backup-rancher2-25-7-2018.tar.gz"
$ docker start rancher
```

Iremos agora acessar o Rancher novamente e iremos verificar que o usuário admin já está configurado, e que nosos ambiente voltou ao seu estado anterior.


# Exercício 25

### DR Kubernetes - Backup

* BACKUPS AUTOMATICOS QUE O KUBERNETES FAZ. 


# Exercício 26

### Rancher HA

Agora iremos fazer uma instalação do Rancher em HA. Para isso iremos usar a instância do worker 1, e a partir dela, que iremos instalar o Rancher na máquina do Rancher Server.

Limpar todos os nodes de todos container rodando, para sanitizar o ambiente todo. 

```sh
$ docker rm -f $(docker ps -qa)
$ docker volume rm $(docker volume ls)
$ systemctl stop docker
$ for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do umount $mount; done
$ rm -rf /etc/ceph /etc/cni /etc/kubernetes /opt/cni /opt/rke /run/secrets/kubernetes.io /run/calico /run/flannel /var/lib/calico /var/lib/etcd /var/lib/cni /var/lib/kubelet /var/lib/rancher/rke/log /var/log/containers /var/log/pods /var/run/calico
$ systemctl start docker
```

Iremos criar o usuario na máquina de destino, o nosso host A - Rancher Server. 

```sh
$ adduser rancher
$ su rancher
$ cd ~/
$ ssh-keygen
```

Foram criadas as chaves públicas e privadas no diretório  **/home/rancher/.ssh/**
```sh
$ cp /home/rancher/.ssh/id_rsa.pub /home/rancher/.ssh/authorized_keys
$ exit
```
Virar sudo e adicionar o usuario ao grupo do docker.
```sh
$ usermod -aG docker rancher
```

Iremos pegar a chave privada que foi criada para o usuário, e adicionar ela nas chaves da máquina de onde iremos rodar os comandos do restore pelo RKE.

Iremos copiar ela, pela tela mesmo.
```sh
$ cat /home/rancher/.ssh/id_rsa
```

E adicionar a chave do usuário linux para **.ssh/id_rsa**, para que o usuário possa se logar e rodar o docker.

Logado na máquina onde iremos rodar os comandos, colar a chave.

```sh
$ adduser rancher
$ su rancher
$ cd ~/

$ vi /home/rancher/chave.pem
$ chmod 600 /home/rancher/chave.pem
$ ssh -i /home/rancher/chave.pem rancher@10.11.24.143
$ exit
```
Para ter finalizado com sucesso, acessamos o terminal da máquina de destino.





### Instalar kubectl no host B - Worker 1

Agora iremos instalar o kubectl, que é a CLI do kubernetes. Através do kubectl é que iremos interagir com o cluster.
```sh
$ cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
$ yum install -y kubectl
$ kubectl
```


### Instalar kubectl no host B - Worker 1

```sh
$ yum install wget -y
$ wget https://github.com/rancher/rke/releases/download/v0.2.6/rke_linux-amd64
$ mv rke_linux-amd64 rke
$ chmod +x rke

$ ./rke
```


Criar o arquivo rancher-cluster.yml


```sh
$ vi rancher-cluster.yml

nodes:
  - address: 10.11.24.143
    # internal_address: 172.16.22.12
    user: rancher
    role: [controlplane,worker,etcd]
    ssh_key_path: /home/rancher/chave.pem
  #- address: 165.227.116.167
    #internal_address: 172.16.32.37
    #user: ubuntu
    #role: [controlplane,worker,etcd]
  #- address: 165.227.127.226
    #internal_address: 172.16.42.73
    #user: ubuntu
    #role: [controlplane,worker,etcd]
services:
  etcd:
    snapshot: true
    creation: 6h
    retention: 24h
```

```sh
$ ./rke up --config ./rancher-cluster.yml
```

Iremos acompanhar o deployment de todo Rancher e do cluster Kubernetes do Rancher.

Ao término, o RKE deverá ter gerado o arquivo **kube_config_rancher-cluster.yml** , que contém as configurações do cluster.

Se pegarmos este arquivo, e usarmos na instância onde instalamos o kubectl, podemos rodar os comandos no cluster.

Com o cluster rodando, iremos para o próximo passo que é instalar o Helm.

```sh
$ mkdir -p ~/.kube
$ cp kube_config_rancher-cluster.yml  ~/.kube/config
$ kubectl get nodes


$ cd /root
$ wget https://get.helm.sh/helm-v2.14.2-linux-amd64.tar.gz
$ tar -xvf helm-v2.14.2-linux-amd64.tar.gz
$ cd linux-amd64
$ kubectl -n kube-system create serviceaccount tiller
$ kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
$ ./helm init --service-account tiller
```

Instalado Rancher

```sh
$ ./helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
```

Iremos ver a versão do Rancher disponível para o Helm
```sh
$ ./helm search rancher
```


Instalar o gerenciador de certificados
```sh
$ ./helm install stable/cert-manager  --name cert-manager --namespace kube-system  --version v0.5.2
```

```sh
$ ./helm install rancher-stable/rancher --name rancher --namespace cattle-system --set hostname=aluno1.rancher.ailoshml.coop.br
```

Irá demorar um certo tempo até o ambiente estar 100% disponível, por isso é preciso aguardar um pouco.




# Exercício 27

### Rancher Upgrade

```sh
$ ./helm init --upgrade --service-account tiller
$ ./helm repo update
$ ./helm repo list
$ ./helm get values rancher
$ ./helm upgrade rancher rancher-latest/rancher --set hostname=aluno1.rancher.ailoshml.coop.br
```


Rancher Labs - Kubernetes Master Class - Three Pillars of K8s Observability

How to Build an Enterprise Kubernetes Strategy -- June 2019

Diving Deep Into Kubernetes Networking



# Exercício 28

### CI/CD - Pipeline


1) Criar conta no Github.
2) Criar conta no DockerHub.
2) Fazer um fork do repositorio de exemplo: https://github.com/jonathanbaraldi/pipeline-example-go
3) Após o fork feito, deixar o git aberto, e entrar no rancher em Treinamento  -> Default > Tools -> Pipeline
4) Intregrar com o Github. -> Register a new application.
5) Encontrar o repositório : https://github.com/<USUARIO>/pipeline-example-go.git, e colocar como Enable
6) Adicionar o Registro - Menu Resources -> Registries
7) Usar o usuario e senha do DockerHub.
8) Voltar para Workloads -> Pipelines.
9) Editar e configurar.
10) Item 3 - Trocar o <Image Name>, colocar o nome do repositorio/usuario do dockerhub.
11) Deixar marcado Docker Hub.
12) Rodar
13) Observar os deployment's.
14) Alterar o fonte no Github, comitar.
15) Rodar novamente o pipeline.






