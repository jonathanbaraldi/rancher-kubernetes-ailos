
# >>>>>>

SEGUNDA 
	TARDE = INTRO

TERÇA
	MANHÃ = INTRO
	TARDE = INTRO
	Exercícios = 1-8 

QUARTA
	MANHÃ = DEV
	TARDE = DEV
	Exercícios = 9-20

QUINTA
	MANHÃ = ADMIN
	TARDE = ADMIN
	Exercícios = 21-24

SEXTA
	MANHÃ = Arquiteturas e melhores práticas




------------------------------------------------------------


harbor.rancher.ailos.coop.br

## >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


# MAQUINAS
svh03300u1adm01 (IP = 10.11.24.143/24)
svh03300u1wrk01 (IP = 10.11.24.144/24)
svh03300u1wrk02 (IP = 10.11.24.145/24)

ssh root@10.11.24.143
ssh root@10.11.24.144
ssh root@10.11.24.145

ssh t0032468@svp03300k8rke01
ssh t0032468@10.11.80.70

ssh t0032468@svp03300k8adm01
ssh t0032468@10.11.80.50
	
	svh03300u1adm01.<> -> tipo A -> 10.11.24.143/24
	svh03300u1wrk01.<> -> tipo A ->  10.11.24.144/24
	svh03300u1wrk02.<> -> tipo A ->  10.11.24.145/24
	
	aluno1.rancher.<> -> CNAME -> svh03300u1adm01.<>
	*.aluno1.rancher.<> -> CNAME -> svh03300u1wrk01.<>
									svh03300u1wrk02.<> 
# >>>>>>


## Agenda
- Containers Docker
- Registro
- Kubernetes

- Arquitetura do Rancher e Documentação do kubernetes na documentação oficial do Rancher.

# TERÇA

#### Exercício 1 
- Verificar ambiente
- Fazer docker login nos host's

#### Exercício 2 
- Fazer build dos containers e rodar aplicação no host
- Cada aluno irá construir as imagens para seus projetos
- Fazer push para o registro

#### Exercício 3
- Instalar Rancher Single Node

#### Exercício 4
- Criar cluster Kubernetes com 2 nós

#### Exercício 5 - Kubectl
- Usar kubectl


#### Exercício 6 - Dns
- Deployment do Traefik

#### Exercício 7 - Log
- Deployment do Graylog

#### Exercício 8 - Monitoramento
- Deployment do Grafana+Prometheus

#### Exercício 9 - App
- Deployment de aplicação no cluster, usando o registro


# >>>>>>>>>>>>>>>>>>>>>>> FINAL INTRO


#### Exercício 10 - CronJob
- Deployment de cronjob

#### Exercício 11 - ConfigMap
- Deployment de configmap

#### Exercício 12 - Secrets
- Deployment de secrets

# QUARTA

#### Exercício 13 - Liveness
- Deployment de aplicação com health-check

#### Exercício 14 - Rolling-Update
- Deployment de aplicação com Rolling-Update

#### Exercício 15 - Autoscaling

#### Exercício 16 - Volume

#### Exercício 17 - Politicas de rede

#### Exercício 18 - Scheduling

#### Exercício 19 - Kubeless

#### Exercício 20 - HELM - Gerenciador de pacotes do kubernetes

# QUINTA

#### Exercício 21 - ResourceQuota

#### Exercício 22 - K8S-Cleaner

#### Exercício 23 - PDF - Networking

#### Exercício 24 - DR Rancher

#### Exercício 25 - DR do Kubernetes

#### Exercício 26 - HA

### Como construir uma estratégia de containers Enterprise.

* Documentos novos

How to Build an Enterprise Kubernetes Strategy -- June 2019.pdf
Kubernetes Master Class - Use persistent storage In Kubernetes and Project Longhorn.pptx
Rancher Labs - Kubernetes Master Class - Three Pillars of K8s Observability.pptx



